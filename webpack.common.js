const path = require("path");
const webpack = require("webpack");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
// 最新的 vue-loader 中，VueLoaderPlugin 插件的位置有所改变
const { VueLoaderPlugin } = require("vue-loader/dist/index");

const AutoImport = require("unplugin-auto-import/webpack");
const Components = require("unplugin-vue-components/webpack");
const { ElementPlusResolver } = require("unplugin-vue-components/resolvers");
const FriendlyErrorsWebpackPlugin = require("friendly-errors-webpack-plugin");

module.exports = () => {
  const entryFile = process.env.TAMPERMONKEY_ENTRY_FILE;
  return {
    entry: {
      app: "./src/main.js",
    },
    output: {
      filename: entryFile,
      publicPath: "/",
      path: path.resolve(__dirname, "dist"),
      hotUpdateChunkFilename: "hot/[id].[hash].hot-update.js",
      hotUpdateMainFilename: "hot/[hash].hot-update.json",
    },
    resolve: {
      alias: {
        "@": path.resolve(__dirname, "src"),
      },
    },
    module: {
      rules: [
        {
          test: /\.mjs$/,
          include: /node_modules/,
          type: "javascript/auto",
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"],
        },
        {
          test: /\.scss$/,
          use: ["style-loader", "css-loader", "sass-loader"],
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: ["file-loader"],
        },
        {
          test: /\.js$/,
          exclude: /(node_modules)/,
          include: path.resolve(__dirname, "src"),
          loader: "babel-loader",
        },
        {
          test: /\.vue$/,
          loader: "vue-loader",
          options: {
            loaders: {
              scss: ["style-loader", "css-loader", "sass-loader"],
            },
          },
        },
      ],
    },
    plugins: [
      new FriendlyErrorsWebpackPlugin(),
      new VueLoaderPlugin(),
      new CleanWebpackPlugin(),
      new webpack.DefinePlugin({
        __APP_NAME__: JSON.stringify(process.env.TAMPERMONKEY_APP_NAME),
        __APP_ENVIRONMENT__: JSON.stringify(
          process.env.TAMPERMONKEY_APP_ENVIRONMENT
        ),
        __APP_VERSION__: JSON.stringify(require("./package.json").version),
      }),
      // new webpack.ProvidePlugin({
      //   $: 'jquery',
      //   jQuery: 'jquery',
      //   'window.jQuery': 'jquery',
      // }),
      AutoImport({
        resolvers: [ElementPlusResolver()],
      }),
      Components({
        resolvers: [ElementPlusResolver()],
      }),
    ],
  };
};
