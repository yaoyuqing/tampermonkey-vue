/*
 * @Description: 
 * @Author: yao yu qing
 * @Date: 2022-01-05 16:18:50
 * @LastEditTime: 2022-01-06 12:02:20
 * @LastEditors: yyq
 * @FilePath: \tampermonkey-vue\babel.config.js
 */
module.exports = {
  presets: ["@babel/preset-env"],
  plugins: [
      "@babel/plugin-transform-runtime",
      "@babel/plugin-syntax-dynamic-import",
      [
        'import',
        { libraryName: 'ant-design-vue', libraryDirectory: 'es', style: 'css' }
      ]
  ],
};
