/*
 * @Description: 规定 commit 的格式
 * @Author: F-Stone
 * @Date: 2021-05-14 16:40:20
 * @LastEditTime: 2021-05-14 16:40:22
 * @LastEditors: F-Stone
 */

/* ---------------------------------- */
/*        commitlint.config.js        */
/* ---------------------------------- */

module.exports = {
    extends: ["@commitlint/config-angular"],
    rules: {
        "type-enum": [
            2,
            "always",
            [
                "🚀  feature",
                "📦  build",
                "🚁  update",
                "📚  docs",
                "🚑  fix",
                "🎨  style",
                "🔨  refactor",
                "📝  perf",
                "🔫  test",
                "⚙️  chore",
                "📥  merge",
                "⏪  revert",
                "🏃  WIP"
            ]
        ]
    },
    parserPreset: {
        parserOpts: {
            headerPattern: /^(.*\s{2}\w+)\((.+)\).*:\s*(.*)\s*$/,
            headerCorrespondence: ["type", "scope", "subject"]
        }
    }
};
