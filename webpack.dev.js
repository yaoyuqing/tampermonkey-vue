/*
 * @Description: 
 * @Author: yao yu qing
 * @Date: 2021-12-20 11:43:27
 * @LastEditTime: 2022-01-07 16:30:06
 * @LastEditors: yyq
 * @FilePath: \tampermonkey-vue\webpack.dev.js
 */
const { merge } = require('webpack-merge')
const common = require('./webpack.common.js')
const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = merge(common(), {
  mode: 'development',
  target: 'web',
  devtool: 'inline-source-map',
  devServer: {
    publicPath: '/',
    contentBase: path.join(__dirname, 'dist'),
    stats: "errors-only",
    hot: true, // hot reload
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods":
            "GET, POST, PUT, DELETE, PATCH, OPTIONS",
        "Access-Control-Allow-Headers":
            "X-Requested-With, content-type, Authorization",
    },
    disableHostCheck: true,
    hotOnly: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'test page'
    }),
    new webpack.HotModuleReplacementPlugin() // hot reload
  ]
})
