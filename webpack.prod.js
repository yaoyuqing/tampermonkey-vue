/*
 * @Description: 
 * @Author: yao yu qing
 * @Date: 2022-01-05 14:58:33
 * @LastEditTime: 2022-01-07 16:31:17
 * @LastEditors: yyq
 * @FilePath: \tampermonkey-vue\webpack.prod.js
 */
const { merge } = require("webpack-merge");
const common = require("./webpack.common");

module.exports = merge(common(), {
  mode: "production",
  stats: {
    // copied from `'minimal'`
    all: false,
    modules: true,
    maxModules: 0,
    errors: true,
    warnings: true,
    // our additional options
    moduleTrace: true,
    errorDetails: true
  },
  externals: {
    // use @require in header to import vue
    vue: "Vue",
    'owl.carousel': "owlCarousel",
    'jquery': '$',
    // 'ant-design-vue': 'ant-design-vue'
  },
});
