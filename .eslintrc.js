/*
 * @Description:
 * @Author: F-Stone
 * @Date: 2021-05-14 17:07:45
 * @LastEditTime: 2021-05-14 17:07:47
 * @LastEditors: F-Stone
 */

module.exports = {
    root: true,
    parserOptions: {
        parser: "@typescript-eslint/parser",
        ecmaVersion: 2020,
        sourceType: "module",
        ecmaFeatures: {
            legacyDecorators: true,
            experimentalObjectRestSpread: true,
        },
    },
    env: {
        es6: true,
        browser: true,
        node: true,
    },
    extends: ["eslint:recommended", "plugin:prettier/recommended"],
    ignorePatterns: [
        "**/dll/**/*.*",
        "/dll/**/*.*",
        "**/lib/**/*.*",
        "/lib/**/*.*",
        "**/public/**/*.*",
        "/public/**/*.*",
    ],
    rules: {
        "prettier/prettier": [
            "error",
            {
                endOfLine: "auto",
            },
        ],
        "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
        "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    },
    overrides: [
        {
            files: ["**/*.js"],
            parser: "@babel/eslint-parser",
            plugins: ["@babel"],
            parserOptions: {
                ecmaFeatures: {
                    legacyDecorators: true,
                    experimentalObjectRestSpread: true,
                },
            },
            extends: ["eslint:recommended", "plugin:prettier/recommended"],
            rules: {
                "no-debugger":
                    process.env.NODE_ENV === "production" ? "warn" : "off",
            },
        },
        {
            files: ["**/*.ts"],
            extends: [
                "eslint:recommended",
                "plugin:@typescript-eslint/recommended",
                "prettier/@typescript-eslint",
                "plugin:prettier/recommended",
            ],
            parserOptions: {
                parser: "@typescript-eslint/parser",
                ecmaVersion: 2020,
                sourceType: "module",
                ecmaFeatures: {
                    legacyDecorators: true,
                    experimentalObjectRestSpread: true,
                },
            },
            rules: {
                "no-debugger":
                    process.env.NODE_ENV === "production" ? "warn" : "off",
            },
        },
        {
            files: ["**/*.d.ts"],
            parserOptions: {
                parser: "@typescript-eslint/parser",
                ecmaVersion: 2020,
                sourceType: "module",
            },
            rules: {
                "@typescript-eslint/no-explicit-any": 0,
            },
        },
    ],
    globals: {
        $: "readonly",
        jQuery: "readonly",
        log: "readonly",
        TimelineLite: "readonly",
        TweenLite: "readonly",
        axios: "readonly",
    },
};
